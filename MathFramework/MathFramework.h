//
//  MathFramework.h
//  MathFramework
//
//  Created by Dani Barca on 30/06/13.
//  Copyleft (ɔ) 2013 Dani Barca.
//

#ifndef MathFramework_MathFramework_h
#define MathFramework_MathFramework_h

#include "Includes.h"
#include "Vector.h"
#include "Matrix.h"

#define PI      3.14159265359
#define DEG2RAD 0.0174532925
#define RAD2DEG 57.2957795

#define DEGTORAD(d) d*DEG2RAD
#define RADTODEG(r) r*RAD2DEG

int round(float x);

#endif
